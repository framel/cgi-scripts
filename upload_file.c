#include <stdlib.h> // getenv, strtoul
#include <unistd.h> // read, write
#include <string.h> // memset, strlen
#include <fcntl.h>  // open, close
#include <errno.h>

#define BUFSIZE 1024

/*
 * Upload file to server with:
 * curl -X POST http://localhost/upload?dest_file --data-binary @"src_file"
 */

void message(char *str)
{
  write(1, str, strlen(str));
}

int receive_data(int fd, size_t len)
{
  char input[BUFSIZE];
  size_t readed;
  size_t writed;
  size_t total;

  readed = 0;
  total = 0;
  writed = 0;
  while (total < len) {
    memset(input, 0, BUFSIZE);
    readed = read(STDIN_FILENO, input, BUFSIZE);
    total = total + readed;
    writed = writed + write(fd, input, readed);
  }
  return (writed == len ? 0 : -1);
}

size_t conv_strtoul(char *str)
{
  char *endptr;
  size_t val;

  errno = 0;
  val = strtoul(str, &endptr, 10);

  if (errno != 0) {
    message("strtoul error");
    exit(EXIT_FAILURE);
  }
  if (endptr == str) {
    message("Error, no digits were found.\n");
    exit(EXIT_FAILURE);
  }
  return (val);
}

int process_request(char *file_path, size_t len)
{
  int fd;
  int ret;

  ret = -1;
  fd = open(file_path, O_CREAT | O_WRONLY | O_EXCL, S_IRUSR | S_IRGRP | S_IROTH);
  if(-1 == fd) {
    message("Opening error, cannot store data.\n");
  } else {
    ret = receive_data(fd, len);
    if (-1 == ret) {
      message("Writing error has occured.\n");
    }
    close(fd);
  }
  return (ret);
}

int main(void)
{
  char *lenstr;
  char *file_path;
  size_t len;
  int ret;

  ret = -1;
  file_path = getenv("QUERY_STRING");
  lenstr = getenv("CONTENT_LENGTH");
  message("Content-Type: text/html; charset=utf-8\r\n\n");
  if (NULL == lenstr || NULL == file_path) {
    message("Error in invocation - wrong FORM probably.\n");
  } else {
    len = conv_strtoul(lenstr);
    if (0 >= len) {
      message("Error POST request is empty.\n");
    } else {
      ret = process_request(file_path, len);
    }
  }
  return (ret);
}
